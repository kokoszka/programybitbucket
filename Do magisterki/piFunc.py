# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 21:35:57 2020

@author: 9
"""

#import rdflib

import owlready2
import types

# Defincja funkcji pi transformującej ontologie opierającą się o SROIQ4 na ontologię w logice SROIQ
#def pi():
#   return

# Komenda otwierające ontologię
onto = owlready2.get_ontology("file://C:/Users/9/Do pracy magisterskiej/ontoPrzyklad1RDF_XML.owl").load() 

#Context manager do pracy nad ontologią
with onto:
    
    for i in onto.classes():        
        splited_name=str(i).split(".")
        class_name=splited_name[1]
        
# Komenda zwracająca nadklasę klasy
#        print("Nadklasą klasy: {} jest klasa: {}.".format(class_name, str(i.is_a).split(".")[1]))
        
# Pętla tworząca primowane klasy       
        if "`" not in class_name:
            types.new_class("{}".format(class_name+"`"), ())  
            
# Komendy do nadawania labeli
#            i.label.append("{}".format(class_name.capitalize()))
#        else:
#            i.label.append("{}".format(class_name.capitalize()))
            
# Pętla do usuwania nadkalas        
#        if class_name=="skalpel":
#            del i.is_a[0]
#            print(i.is_a)
        
# Pętla na wypadek za dużej iloci klas        
#        if '`' in str(i):
#            destroy_entity(i)
        
# Komenda usuwająca wszystkie labele       
#        i.label=[]   
        
# Wywietlanie listy klas w ontologii
        print('-'+class_name)
        
# Komenda zapisująca ontologię
onto.save(file = "ontoPrzyklad1RDF_XML.owl", format = "rdfxml")
